'use strict'

module.exports = function(grunt) {

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        pkg: grunt.file.readJSON('package.json'),

        uglify: {
            config: {
                src: 'src/modules/config.js',
                dest: 'js/config.min.js'
            },
            helpers: {
                src: 'src/modules/helpers.js',
                dest: 'js/helpers.min.js'
            },
            mainApp: {
                src: ['src/modules/main/mainApp.js', 'src/modules/main/controllers/*.js', 'src/modules/main/services/*.js'],
                dest: 'js/mainApp.min.js'
            },
            AuthModule: {
                src: ['src/modules/auth/AuthModule.js', 'src/modules/auth/controllers/*.js', 'src/modules/auth/services/*.js'],
                dest: 'js/AuthModule.min.js'
            },
            ConcesionModule: {
                src: ['src/modules/concesion/ConcesionModule.js', 'src/modules/concesion/controllers/*.js', 'src/modules/concesion/services/*.js'],
                dest: 'js/ConcesionModule.min.js'
            },
            UsuarioModule: {
                src: ['src/modules/usuario/module.js', 'src/modules/usuario/controllers/*.js', 'src/modules/usuario/services/*.js'],
                dest: 'js/UsuarioModule.min.js'
            },
            TaxiModule: {
                src: ['src/modules/taxi/module.js', 'src/modules/taxi/controllers/*.js', 'src/modules/taxi/services/*.js'],
                dest: 'js/TaxiModule.min.js'
            },
            ClienteModule: {
                src: ['src/modules/cliente/module.js', 'src/modules/cliente/controllers/*.js', 'src/modules/cliente/services/*.js'],
                dest: 'js/ClienteModule.min.js'
            },
            SolicitudModule: {
                src: ['src/modules/solicitud/module.js', 'src/modules/solicitud/controllers/*.js', 'src/modules/solicitud/services/*.js'],
                dest: 'js/SolicitudModule.min.js'
            },
            ConductorModule: {
                src: ['src/modules/conductor/module.js', 'src/modules/conductor/controllers/*.js', 'src/modules/conductor/services/*.js'],
                dest: 'js/ConductorModule.min.js'
            },
        },

         concat: {
            dist: {
                src: [
                    'js/config.min.js', 
                    'js/helpers.min.js', 
                    'js/mainApp.min.js', 
                    'js/AuthModule.min.js', 
                    'js/ConcesionModule.min.js', 
                    'js/UsuarioModule.min.js', 
                    'js/TaxiModule.min.js', 
                    'js/ClienteModule.min.js', 
                    'js/SolicitudModule.min.js', 
                    'js/ConductorModule.min.js'
                ],
                dest: 'js/app.min.js'
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'css/app.min.css': 'src/css/app.scss',
                }
            }
        },

        watch: {
            js: {
                files: ['src/modules/**/*.js'],
                tasks: ['uglify', 'concat']
            },
            styles: {
                files: ['src/css/app.scss'],
                tasks: ['sass']
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'sass',
        'uglify',
        'concat',
        'watch'
    ]);
};
