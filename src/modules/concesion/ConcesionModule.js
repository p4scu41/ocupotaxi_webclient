'use strict';
/* globals angular, Config, Helpers */

var ConcesionModule = angular.module('ConcesionModule', [
        'md.data.table', /* https://github.com/daniel-nagy/md-data-table */
        'ui.mask', /* https://github.com/angular-ui/ui-mask */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('concesiones_index', {
                url: '/concesiones',
                templateUrl: 'src/modules/concesion/views/index.html',
                controller: 'ConcesionController',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state('concesiones_create', {
                url: '/concesiones/create',
                templateUrl: 'src/modules/concesion/views/create.html',
                controller: 'ConcesionController',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state('concesiones_edit', {
                url: '/concesiones/:id/edit',
                templateUrl: 'src/modules/concesion/views/edit.html',
                controller: 'ConcesionController',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];

    }]).run(['$rootScope', function($rootScope){
        
    }]);
