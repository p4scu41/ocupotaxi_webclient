'use strict';
/* globals ConcesionModule */

ConcesionModule.service('ConcesionService', ['Restangular', function(Restangular){
    return Restangular.service('concesiones');
}]);