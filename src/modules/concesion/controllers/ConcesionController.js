'use strict';
/* globals angular, _ */

angular.module('ConcesionModule')
    .controller('ConcesionController', ['$scope', '$state', 'MenuService', 'ToastService', 'ConcesionService', 'Restangular',
        function ($scope, $state, MenuService, ToastService, ConcesionService, Restangular) {
            $scope.menu = MenuService;
            $scope.concesiones = [];
            $scope.municipios = []; // Catálogo de municipios para create y edit
            $scope.concesion = {}; // Por default, creamos un concenionario vacio
            $scope.promise = null; // Muestra el progress-linear
            $scope.selected = [];
            $scope.paginator = {
                'total': 0,
                'per_page': 15,
                'current_page': 1,
                'last_page': 0
            };

            $scope.goto = function (state, params) {
                $state.go(state, params);
            };
            
            $scope.list = function(page, limit) {
                $scope.paginator.current_page = page || 1;

                $scope.promise = ConcesionService.getList({page: $scope.paginator.current_page}).then(function (concesiones) {
                    // Copiamos los metadatos del paginador
                    $scope.paginator = _.extend($scope.paginator, _.pick(concesiones.meta.extra, 'total', 'per_page', 'last_page'));
                    $scope.concesiones = concesiones;
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.toogleActivo = function (concesionario, $event) {
                $event.stopPropagation();
                $event.preventDefault();

                concesionario.activo = (!concesionario.activo) ? 1 : 0;
                $scope.promise = concesionario.put().then(function (concesionario) {
                    ToastService.success('Concesionario actualizado exitosamente.');
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.store = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                // Si el formulario es correcto, procedemos a enviar los datos
                $scope.promise = true;

                $scope.concesion.fecha_concesion = moment($scope.concesion.fecha_concesion, 'DD-MM-YYYY').format('YYYY-MM-DD');
                $scope.concesion.fecha_publicacion = moment($scope.concesion.fecha_publicacion, 'DD-MM-YYYY').format('YYYY-MM-DD');

                ConcesionService.post($scope.concesion).then(function () {
                    ToastService.success('Datos guardados exitosamente.').then(function () {
                        $state.go('concesiones_index');
                    });
                }, function () {
                    ToastService.errorMsg();
                });
            };

            $scope.update = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }
                
                $scope.promise = true;
                $scope.concesion.fecha_concesion = moment($scope.concesion.fecha_concesion, 'DD-MM-YYYY').format('YYYY-MM-DD');
                $scope.concesion.fecha_publicacion = moment($scope.concesion.fecha_publicacion, 'DD-MM-YYYY').format('YYYY-MM-DD');
                
                $scope.concesion.put().then(function(concesion) {
                    ToastService.success('Datos actualizados exitosamente.').then(function () {
                        $state.go('concesiones_index');
                    });
                }, function (response) {
                    ToastService.errorMsg();
                    $scope.promise = false;
                });
            };

            $scope.pageChanged = function (page, limit) {
                $scope.paginator.current_page = page;
                $scope.list($scope.paginator.current_page);
            };

            // Si la ruta que se esta ejecutando es el index
            if ($state.is('concesiones_index')) {
                // Cargamos la lista de concesiones
                $scope.list();
            }

            // Si estamos creando o actualizando, es necesario cargar el catálogo de municipios
            if ($state.is('concesiones_create')) {
                $scope.promise = true;
                Restangular.all('municipios').getList().then(function(municipios) {
                  $scope.municipios = municipios;
                  $scope.promise = false;
                });
            }

            // Si estamos actualizando, es necesario obtener el elemento a editar
            if ($state.is('concesiones_edit')) {
                $scope.promise = true;
                
                Restangular.all('municipios').getList().then(function(municipios) {
                  $scope.municipios = municipios;
                });

                ConcesionService.one($state.params.id).get().then(function(concesion) {
                    $scope.concesion = concesion;
                    // Se convierte las fechas en objeto Date
                    //$scope.concesion.fecha_concesion = new Date($scope.concesion.fecha_concesion + ' 00:00:00.000');
                    //$scope.concesion.fecha_publicacion = new Date($scope.concesion.fecha_publicacion + ' 00:00:00.000');
                    $scope.concesion.fecha_concesion = moment($scope.concesion.fecha_concesion, 'YYYY-MM-DD').format('DD-MM-YYYY');
                    $scope.concesion.fecha_publicacion = moment($scope.concesion.fecha_publicacion, 'YYYY-MM-DD').format('DD-MM-YYYY');
                    $scope.promise = false;
                }, function (response) {
                    ToastService.error('Error al obtener los datos, intentelo nuevamente');
                    $scope.promise = false;
                });
            }
        }
    ]);
