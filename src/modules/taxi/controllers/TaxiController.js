'use strict';
/* globals angular, _, Helpers, io, Config */

angular.module('TaxiModule')
    .controller('TaxiController', ['$scope', '$state', 'MenuService', 'ToastService', 'TaxiService', '$http', 'Restangular', 'uiGmapGoogleMapApi', '$timeout',
        function ($scope, $state, MenuService, ToastService, TaxiService, $http, Restangular, uiGmapGoogleMapApi, $timeout) {
            var resource = 'taxis';
            var socket = {};
            var storage = $.localStorage;
            // Bandera que nos permite saber si estabamos visualizando el mapa
            var runningMapa = $state.is(resource + '_map');

            $scope.menu = MenuService;
            $scope.list = [];
            $scope.concesionarios = []; // Lista de concesionarios para filtrar
            $scope.concesionarioSelected = null; // Concesionario seleccionado
            $scope.statusSelected = null; // Status seleccionado
            $scope.filtroNoEconomico = null;
            $scope.catalogos = []; // Catálogos para create y edit
            $scope.model = {}; // Por default, creamos un modelo vacío
            $scope.promise = null; // Muestra el progress-linear
            $scope.paginator = {
                'total': 0,
                'per_page': 15,
                'current_page': 1,
                'last_page': 0
            };
            $scope.map = {
                center: {  // Centramos en Tuxtla
                    latitude: 16.7516157,
                    longitude: -93.1254237
                },
                zoom: 13,
                control: {},
                markers: []
            };

            $scope.goto = function (state, params) {
                $state.go(state, params);
            };

            $scope.getList = function(page) {
                $scope.paginator.current_page = page || 1;

                $scope.promise = TaxiService.getList({
                        page: $scope.paginator.current_page,
                        concesionario_id: $scope.concesionarioSelected,
                        numero_economico: $scope.filtroNoEconomico,
                    }).then(function (list) {
                        // Copiamos los metadatos del paginador
                        $scope.paginator = _.extend($scope.paginator, _.pick(list.meta.extra, 'total', 'per_page', 'last_page'));
                        $scope.list = list;
                    }, function (response) {
                        ToastService.errorMsg();
                    });
            };

            $scope.toogleActivo = function (model, $event) {
                $event.stopPropagation();
                $event.preventDefault();

                model.activo = (!model.activo) ? 1 : 0;
                $scope.promise = model.put().then(function (model) {
                    ToastService.success('Datos actualizados exitosamente.');
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.store = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                // Si el formulario es correcto, procedemos a enviar los datos
                $scope.promise = true;
                // Convertimos la fecha al formato válido d MySQL
                $scope.model.vigencia_poliza = moment($scope.model.vigencia_poliza, 'DD-MM-YYYY').format('YYYY-MM-DD');

                TaxiService.post($scope.model).then(function () {
                    ToastService.success('Datos guardados exitosamente.').then(function () {
                        $state.go(resource + '_index');
                    });
                }, function () {
                    ToastService.errorMsg();
                    $scope.promise = false;
                });
            };

            $scope.update = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                $scope.promise = true;
                // Convertimos la fecha al formato válido d MySQL
                $scope.model.vigencia_poliza = moment($scope.model.vigencia_poliza, 'DD-MM-YYYY').format('YYYY-MM-DD');

                $scope.model.put().then(function(model) {
                    ToastService.success('Datos actualizados exitosamente.').then(function () {
                        $state.go(resource + '_index');
                    });
                }, function (response) {
                    ToastService.errorMsg();
                    $scope.promise = false;
                });
            };

            $scope.pageChanged = function (page, limit) {
                $scope.paginator.current_page = page;
                $scope.getList($scope.paginator.current_page);
            };

            $scope.findTaxiMap = function () {
                if ($scope.concesionarioSelected == 0 && $scope.filtroNoEconomico == '') {
                    ToastService.error('Debe seleccionar un concesionario o número económico.');
                    return false;
                }

                // Filtramos por concesionario
                if ($scope.concesionarioSelected != 0) {
                    $scope.promise = true;

                    // Obtenemos la lista de taxis del concesionario seleccionado
                    TaxiService.getList({
                        concesionario_id: $scope.concesionarioSelected
                    }).then(function (list) {
                        // Recorremos cada uno de los taxis que se estan visualizando en el mapa
                        _.map($scope.map.markers, function(marker) {
                            // Recorremos cada uno de los taxis que pertenecen al concesionario
                            var indexMarker = _.findIndex(list, function (item) {
                                return marker.options.labelContent == item.numero_economico;
                            });

                            // Si el taxi pertenece al concesionario, lo mostramos
                            if (indexMarker !== -1) {
                                $scope.map.markers[indexMarker].options.visible = true;
                            } else {
                                // de lo contrario lo ocultamos
                                marker.options.visible = false;
                            }

                            $scope.promise = false;
                        });
                    }, function (response) {
                        ToastService.errorMsg();
                        $scope.promise = false;
                    });
                } else {
                    $timeout(function () {
                        // Si no esta buscando por concesionario, visualizamos todos
                        _.map($scope.map.markers, function(marker) {
                            marker.options.visible = true;
                        });
                    });
                }

                // Filtramos por número económico
                if ($scope.filtroNoEconomico != '') {
                    var indexMarker = _.findIndex($scope.map.markers, function (marker) {
                        return marker.options.labelContent == $scope.filtroNoEconomico;
                    });

                    if (indexMarker !== -1) {
                        $scope.map.center.latitude = $scope.map.markers[indexMarker].coords.latitude;
                        $scope.map.center.longitude = $scope.map.markers[indexMarker].coords.longitude;

                        $scope.map.markers[indexMarker].options.visible = true;
                        $scope.map.markers[indexMarker].options.animation = 1;

                        $timeout(function () {
                            $scope.map.markers[indexMarker].options.animation = 0;
                        }, 4000);

                        $scope.map.control.getGMap().setZoom(16);
                    }

                    $scope.promise = false;
                }

                // Filtramos por status
                if ($scope.statusSelected != 0) {
                    // Recorremos cada uno de los taxis que se estan visualizando en el mapa
                    _.map($scope.map.markers, function(marker) {
                        // Si el status del taxi es igual al seleccionado, mostramos el marker
                        if (marker.status == $scope.statusSelected) {
                            marker.options.visible = true;
                        } else {
                            // de lo contrario lo ocultamos
                            marker.options.visible = false;
                        }

                        $scope.promise = false;
                    });
                }
            };

            $scope.mapBuilder = function () {
                $scope.promise = true;

                uiGmapGoogleMapApi.then(function(maps) {
                    // Válidamos que exista el token para el servidor de tracking
                    if (storage.isEmpty('token_tracking')) {
                        ToastService.error('El token de acceso para el Servidor de Tracking no es válido, inicie sesión nuevamente.');
                        $scope.promise = false;
                        return false;
                    }

                    // Nos conectamos al servidor de tracking con el namespace admin
                    socket = io(Config.Urls.tracking + '/admin', {
                        'query': 'token=' + storage.get('token_tracking')
                    });

                    // Agregamos listener para los eventos mas comunes
                    socket.on('connect', function() {
                        console.log('socket: connect');
                    });
                    socket.on('error', function(data) {
                        ToastService.error('Error en el Servidor de Tracking.');
                        console.log('socket: error');
                        console.log(data);
                    });
                    socket.on('connect_error', function(data){
                        ToastService.error('Error en la conexión con el Servidor de Tracking.');
                        console.log('socket: connect_error');
                        console.log(data);
                    });
                    socket.on('connect_timeout', function(data){
                        ToastService.error('Error en la conexión con el Servidor de Tracking.');
                        console.log('socket: connect_timeout');
                        console.log(data);
                    });
                    socket.on('disconnect', function() {
                        console.log('socket: disconnect');
                    });

                    // Escuchamos el evento cuando un taxista actualiza su posición
                    socket.on('posicion-taxi-actualizada', function(data) {
                        if (typeof(data) == 'string') {
                            data = jQuery.parseJSON(data);
                        }
                        //console.log('posicion-taxi-actualizada');
                        //console.log(data);

                        if (typeof(data.conductor_id) != 'undefined') {
                            // El wrapper timeout sirve para que se pueda actualizar la vista de
                            // los marker, de lo contrario no se vee en tiempo real los cambios
                            // de posición de cada taxi
                            $timeout(function(){
                                // Si el Taxi ya esta en el listado
                                // actualizamos su posicion
                                var indexMarker = _.findIndex($scope.map.markers, {idKey: data.conductor_id});

                                if (indexMarker !== -1) {
                                    $scope.map.markers[indexMarker].coords.latitude = data.latitud;
                                    $scope.map.markers[indexMarker].coords.longitude = data.longitud;
                                    $scope.map.markers[indexMarker].icon = Config.iconsTaxi[data.status];
                                }
                                // De lo contrario, lo insertamos al arreglo
                                else {
                                    $scope.map.markers.push(
                                        Helpers.markerTaxi(
                                            data.conductor_id,
                                            data.latitud,
                                            data.longitud,
                                            data.status,
                                            data.numero_economico,
                                            data // Se envían todos los valores al infoWindow
                                        )
                                    );
                                }

                                // Si solo es un Taxi el que se esta visualizando
                                // se centra la posición del mapa en su ubicación
                                if ($scope.map.markers.length == 1) {
                                    $scope.map.center.latitude = data.latitud;
                                    $scope.map.center.longitude = data.longitud;
                                }
                            });
                        }
                    });

                    // Escuchamos el evento cuando un taxista se desconecta
                    socket.on('taxi-desconectado', function(data) {
                        if (typeof(data) == 'string') {
                            data = jQuery.parseJSON(data);
                        }

                        //console.log('taxi-desconectado');
                        //console.log(data);

                        if (typeof(data.conductor_id) != 'undefined') {
                            // El wrapper timeout sirve para que se pueda actualizar la vista de
                            // los marker, de lo contrario no se vee en tiempo real los cambios
                            // de posición de cada taxi
                            $timeout(function(){
                                var indexMarker = _.findIndex($scope.map.markers, {idKey: data.conductor_id});

                                if (indexMarker !== -1) {
                                    // Eliminamos el taxi del array
                                    $scope.map.markers.splice(indexMarker, 1);
                                }
                            });
                        }
                    });

                    $scope.promise = false;
                });
            };

            // listener para los cambios de secciones
            $scope.$on('$locationChangeStart', function () {
                // Desconectamos el socket cuando cambiamos a una vista del mapa y
                // en caso de que este conectado
                if (runningMapa && !_.isEmpty(socket)) {
                    if (socket.connected) {
                        socket.disconnect();
                    }
                }
            });

            // Si la ruta que se esta ejecutando es el index
            if ($state.is(resource + '_index')) {
                // Cargamos la lista de elementos
                $scope.getList();
                // Cargamos la lista de concesionarios
                Restangular.all('concesiones').getList().then(function(concesionarios) {
                    $scope.concesionarios = concesionarios;
                });
            }

            // Si estamos creando es necesario cargar los catálogos necesarios
            if ($state.is(resource + '_create')) {
                $scope.promise = true;

                $http.get(Config.Urls.api + '/' + resource + '/catalogos').then(function(response) {
                    $scope.catalogos = response.data.data;
                    $scope.promise = false;
                });
            }

            // Si estamos actualizando, es necesario obtener el elemento a editar
            if ($state.is(resource + '_edit')) {
                $scope.promise = true;

                $http.get(Config.Urls.api + '/' + resource + '/catalogos').then(function(response) {
                    $scope.catalogos = response.data.data;
                });

                // El id del elemento se envía en la ruta
                TaxiService.one($state.params.id).get().then(function(model) {
                    $scope.promise = false;
                    $scope.model = model;
                    //$scope.model.vigencia_poliza = new Date($scope.model.vigencia_poliza + ' 00:00:00.000');
                    $scope.model.vigencia_poliza = moment($scope.model.vigencia_poliza, 'YYYY-MM-DD').format('DD-MM-YYYY');
                    $scope.model.poliza_seguro = parseFloat($scope.model.poliza_seguro);
                    $scope.model.modelo = parseFloat($scope.model.modelo);
                    $scope.model.capacidad = parseFloat($scope.model.capacidad);
                }, function (response) {
                    ToastService.error('Error al obtener los datos, intentelo nuevamente');
                    $scope.promise = false;
                });
            }

            // Si estamos visualizando el mapa
            if ($state.is(resource + '_map')) {
                // Cargamos la lista de concesionarios
                Restangular.all('concesiones').getList().then(function(concesionarios) {
                    $scope.concesionarios = concesionarios;
                });
                // Cargamos el mapa
                $scope.mapBuilder();
            }
        }
    ]);
