'use strict';
/* globals angular, Config, Helpers */

var TaxiModule = angular.module('TaxiModule', [
        'md.data.table', /* https://github.com/daniel-nagy/md-data-table */
        'uiGmapgoogle-maps', /* http://angular-ui.github.io/angular-google-maps */
        'ui.mask', /* https://github.com/angular-ui/ui-mask */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, uiGmapGoogleMapApiProvider) {
        var resource = 'taxis';
        var module = 'taxi';

        $stateProvider
            .state(resource + '_index', {
                url: '/' + resource,
                templateUrl: 'src/modules/' + module + '/views/index.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_create', {
                url: '/' + resource + '/create',
                templateUrl: 'src/modules/' + module + '/views/create.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_edit', {
                url: '/' + resource + '/:id/edit',
                templateUrl: 'src/modules/' + module + '/views/edit.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_map', {
                url: '/' + resource + '/map',
                templateUrl: 'src/modules/' + module + '/views/map.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];

        // Se carga la librería de google maps
        // con las opciones por default
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDQyC7ZZjhdvkKAehMUETTkn9HjtZESUrE',
            /*v: '3.22',
            libraries: 'weather,geometry,visualization'*/
        });

    }]);
