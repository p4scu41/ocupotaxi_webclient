'use strict';

TaxiModule.service('TaxiService', ['Restangular', function(Restangular){
    return Restangular.service('taxis');
}]);