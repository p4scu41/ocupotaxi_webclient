'use strict';
/* globals UsuarioModule */

UsuarioModule.service('UsuarioService', ['Restangular', function(Restangular){
    return Restangular.service('usuarios');
}]);