'use strict';
/* globals angular, _ */

angular.module('UsuarioModule')
    .controller('UsuarioController', ['$scope', '$state', 'MenuService', 'ToastService', 'UsuarioService', 'Restangular',
        function ($scope, $state, MenuService, ToastService, UsuarioService, Restangular) {
            var resource = 'usuarios';
            $scope.required_password = false;
            $scope.menu = MenuService;
            $scope.list = [];
            $scope.municipios = []; // Catálogo de municipios para create y edit
            $scope.model = {}; // Por default, creamos un modelo vacío
            $scope.promise = null; // Muestra el progress-linear
            $scope.paginator = {
                'total': 0,
                'per_page': 15,
                'current_page': 1,
                'last_page': 0
            };

            $scope.goto = function (state, params) {
                $state.go(state, params);
            };
            
            $scope.list = function(page, limit) {
                $scope.paginator.current_page = page || 1;

                $scope.promise = UsuarioService.getList({page: $scope.paginator.current_page}).then(function (list) {
                    // Copiamos los metadatos del paginador
                    $scope.paginator = _.extend($scope.paginator, _.pick(list.meta.extra, 'total', 'per_page', 'last_page'));
                    $scope.list = list;
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.toogleActivo = function (model, $event) {
                $event.stopPropagation();
                $event.preventDefault();

                model.activo = (!model.activo) ? 1 : 0;
                $scope.promise = model.put().then(function (model) {
                    ToastService.success('Datos actualizados exitosamente.');
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.store = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                if ($scope.model.password != $scope.model.password_confirm) {
                    ToastService.error('Revise la contraseña, debe concidir en el campo de confirmación');
                    return false;
                }

                // Creamos el usuario como Administrador
                $scope.model.rol_id = 1;

                // Si el formulario es correcto, procedemos a enviar los datos
                $scope.promise = true;
                UsuarioService.post($scope.model).then(function () {
                    ToastService.success('Datos guardados exitosamente.').then(function () {
                        $state.go(resource + '_index');
                    });
                }, function () {
                    ToastService.errorMsg();
                    $scope.promise = false;
                });
            };

            $scope.update = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                // Si esta actualizando el password, lo validamos
                if ($scope.model.password != '') {
                    if ($scope.model.password != $scope.model.password_confirm) {
                        ToastService.error('Revise la contraseña, debe concidir en el campo de confirmación');
                        return false;
                    }
                }

                $scope.promise = true;
                $scope.model.put().then(function(model) {
                    ToastService.success('Datos actualizados exitosamente.').then(function () {
                        $state.go(resource + '_index');
                    });
                }, function (response) {
                    ToastService.errorMsg();
                    $scope.promise = false;
                });
            };

            $scope.pageChanged = function (page, limit) {
                $scope.paginator.current_page = page;
                $scope.list($scope.paginator.current_page);
            };

            // Si la ruta que se esta ejecutando es el index
            if ($state.is(resource + '_index')) {
                // Cargamos la lista de elementos
                $scope.list();
            }

            // Solo para el caso de registrar, es obligatorio el password
            if ($state.is(resource + '_create')) {
                $scope.required_password = true;
            }

            // Si estamos creando es necesario cargar el catálogo de municipios
            if ($state.is(resource + '_create')) {
                $scope.promise = true;
                Restangular.all('municipios').getList().then(function(municipios) {
                    $scope.municipios = municipios;
                    $scope.promise = false;
                });
            }

            // Si estamos actualizando, es necesario obtener el elemento a editar
            if ($state.is(resource + '_edit')) {
                $scope.promise = true;
                Restangular.all('municipios').getList().then(function(municipios) {
                    $scope.municipios = municipios;
                });

                // El id del elemento se envía en la ruta
                UsuarioService.one($state.params.id).get().then(function(model) {
                    $scope.promise = false;
                    $scope.model = model;
                }, function (response) {
                    ToastService.error('Error al obtener los datos, intentelo nuevamente');
                    $scope.promise = false;
                });
            }
        }
    ]);
