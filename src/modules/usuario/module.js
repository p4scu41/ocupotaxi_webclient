'use strict';
/* globals angular, Config, Helpers */

var UsuarioModule = angular.module('UsuarioModule', [
        'md.data.table' /* https://github.com/daniel-nagy/md-data-table */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', function ($stateProvider) {
        var resource = 'usuarios';
        var module = 'usuario';

        $stateProvider
            .state(resource + '_index', {
                url: '/' + resource,
                templateUrl: 'src/modules/' + module + '/views/index.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_create', {
                url: '/' + resource + '/create',
                templateUrl: 'src/modules/' + module + '/views/create.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_edit', {
                url: '/' + resource + '/:id/edit',
                templateUrl: 'src/modules/' + module + '/views/edit.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];
    }]);
