'use strict';
/* globals angular */

angular.module('AuthModule')
    .controller('AuthController', ['$scope', '$state', 'MenuService', 'ToastService', 'AuthService', '$http', 
        function ($scope, $state, MenuService, ToastService, AuthService, $http) {
            var vm = this;

            $scope.menu = MenuService;

            if ($state.is('logout')) {
                AuthService.logout();
            }

            vm.login = function() {
                var credentials = {
                    email: vm.email,
                    password: vm.password,
                    appweb: true,
                };

                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }
                
                AuthService.login(credentials);
            };

            vm.logout = function() {
                AuthService.logout();
            };

            vm.recuperapass = function() {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                $http.post(Config.Urls.api.replace('admin', '') + 'password/email', {email: vm.email})
                    .then(function () {
                        ToastService.success('Se ha enviado un correo electrónico con <br>las instrucciones para recuperar su contraseña.');
                    }, function () {
                        ToastService.errorMsg();
                    });
            };
        }
    ]);
