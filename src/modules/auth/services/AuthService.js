'use strict';
/* globals AuthModule */

/**
 * Auth Service Provider
 */
AuthModule.factory('AuthService', ['$auth', '$state', 'ToastService', '$http',
    function($auth, $state, ToastService, $http) {
        var storage = $.localStorage;
        
        return {
            login: function (credentials, success, error) {
                $auth.login(credentials).then(function(response) {
                        if (success instanceof Object) {
                            success(response);
                        }

                        storage.set('token_tracking', response.data.data.token_tracking);

                        // Obtenemos el nombre del usuario
                        $http.get(Config.Urls.api.replace('admin', '') + 'userjwt').then(function(response) {
                            storage.set('nombre_usr', response.data.data.nombre);
                        });
                        
                        // If login is successful, redirect to home
                        $state.go('inicio', {});
                    }, function (response) {
                        ToastService.error(response.data.message);

                        if (error instanceof Object) {
                            error(response);
                        }
                    });
            },
            logout: function () {
                $auth.logout().then(function () {
                    storage.remove('token_tracking');
                    storage.remove('nombre_usr');
                    $state.go('login', {});
                });
            }
        };
}]);