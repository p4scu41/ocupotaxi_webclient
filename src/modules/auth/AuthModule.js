'use strict';
/* globals angular, Config, Helpers */

/**
 * @ngdoc overview
 * @name AuthModule
 * @description
 * # AuthModule
 *
 * Main module of the application.
 */
var AuthModule = angular.module('AuthModule', [
        'satellizer', /* https://github.com/sahat/satellizer */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', '$authProvider', function ($stateProvider, $authProvider) {
        // Satellizer -> JSON Web Tokens (JWT)
        // configuration that specifies which API
        // route the JWT should be retrieved from
        // Se elimina la palabra admin, ya que el loguin
        // esta el el root del api
        $authProvider.loginUrl = Config.Urls.api.replace('/admin', '') + Config.Urls.authToken;
        // set the token parent element if the token is not the JSON root
        $authProvider.tokenRoot = 'data';

        $stateProvider
            .state('login', {
                url: Config.Urls.login,
                templateUrl: 'src/modules/auth/views/login.html',
                controller: 'AuthController as auth',
                resolve: {
                    skipIfLoggedIn: Helpers.skipIfLoggedIn
                }
            })
            .state('logout', {
                url: '/logout',
                controller: 'AuthController as auth'
            })
            .state('recuperapass', {
                url: '/recuperapass',
                controller: 'AuthController as auth',
                templateUrl: 'src/modules/auth/views/recuperapass.html',
            });

        /*RestangularProvider.addElementTransformer('users', true, function(user) {
            // This will add a method called login that will do a POST to the path login
            // signature is (name, operation, path, params, headers, elementToPost)
            user.addRestangularMethod('login', 'post', Config.Urls.authToken);

            return user;
        });*/

        // Para evitar errores al ejecutar uglify
        Helpers.skipIfLoggedIn.$inject = ['$q', '$auth'];

    }]).run(['$rootScope', function($rootScope){
        
    }]);
