'use strict'

var Config = {
    /**
     * Main name of the Angular Module, it has to be set in the HTML
     * @type string
     */
    appName: 'mainApp',
    /**
     * Determine if the messages of errors will be show in console
     *
     * @type boolean
     */
    debug: false,

    Urls: {
        /**
         * Home URL of the web api
         *
         * @type string
         */
        //api: 'http://localhost/ocupotaxi_webapi/public/api/v1/admin', // Dev
        //api: 'http://ocupotaxiapi.importare.mx/public/api/v1/admin', // Test
        api: 'https://ocupotaxi.net/api/public/api/v1/admin', // Prod

        /**
         * Home URL of the web client
         *
         * @type string
         */
        //web: 'http://localhost/ocupotaxi_webapi/public', // Dev
        //web: 'http://ocupotaxiwebclient.importare.mx', // Test
        web: 'https://ocupotaxi.net/webclient/', // Prod

        /**
         * URL of the web client
         *
         * @type string
         */
        tracking: 'https://ocupotaxi.herokuapp.com',

        /**
         * Path login view in the frontend
         *
         * @type string
         */
        login: '/login',

        /**
         * Path login to the route in the backend
         *
         * @type string
         */
        authToken: '/authjwt',

        /**
         * Path refresh to the route in the backend
         *
         * @type string
         */
        refreshToken: '/refreshjwt',
    },

    iconsTaxi: [
        {url: 'img/pin.png'}, // 0: No implementado
        {url: 'img/PinTaxista.png'}, // 1: Disponible
        {url: 'img/PinTaxistaOcupado.png'}, // 2: Ocupado
        {url: 'img/PinTaxistaPanico.png'}, // 3: Pánico
        //free: {url: 'http://maps.google.com/mapfiles/kml/pal4/icon62.png'},
    ],

};

// Configuraciones para el plugins Offline
Offline.options = {
    checkOnLoad: true,
    interceptRequests: false,
};
