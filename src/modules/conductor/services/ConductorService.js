'use strict';
/* globals ConductorModule */

ConductorModule.service('ConductorService', ['Restangular', function(Restangular){
    return Restangular.service('conductores');
}]);