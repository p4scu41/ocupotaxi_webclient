'use strict';
/* globals angular, _ */

angular.module('ConductorModule')
    .controller('ConductorController', ['$scope', '$state', 'MenuService', 'ToastService', 'ConductorService', 'Restangular', '$filter', 'TaxiService',
        function ($scope, $state, MenuService, ToastService, ConductorService, Restangular, $filter, TaxiService) {
            var resource = 'conductores';
            $scope.required_password = false;
            $scope.menu = MenuService;
            $scope.list = [];
            $scope.municipios = []; // Catálogo de municipios para create y edit
            $scope.concesionarios = []; // Lista de concesionarios para filtrar
            $scope.loadingTaxis = false;
            $scope.taxis = [];
            $scope.model = {}; // Por default, creamos un modelo vacío
            $scope.promise = null; // Muestra el progress-linear
            $scope.paginator = {
                'total': 0,
                'per_page': 15,
                'current_page': 1,
                'last_page': 0
            };

            $scope.goto = function (state, params) {
                $state.go(state, params);
            };
            
            $scope.getList = function(page, limit) {
                $scope.paginator.current_page = page || 1;

                $scope.promise = ConductorService.getList({page: $scope.paginator.current_page}).then(function (list) {
                    // Copiamos los metadatos del paginador
                    $scope.paginator = _.extend($scope.paginator, _.pick(list.meta.extra, 'total', 'per_page', 'last_page'));
                    $scope.list = list;
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.toogleActivo = function (model, $event) {
                $event.stopPropagation();
                $event.preventDefault();

                model.usuario.activo = (!model.usuario.activo) ? 1 : 0;
                $scope.promise = Restangular.one('usuarios', model.usuario_id)
                    .customPUT({activo: model.usuario.activo}).then(function (model) {
                        ToastService.success('Datos actualizados exitosamente.');
                    }, function (response) {
                        ToastService.errorMsg();
                    });
            };

            $scope.store = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                if ($scope.model.password != $scope.model.password_confirm) {
                    ToastService.error('Revise la contraseña, debe concidir en el campo de confirmación');
                    return false;
                }

                // Creamos el usuario como Conductor
                $scope.model.rol_id = 2;
                // Copiamos el objeto model
                // porque al transformar las fechas a string
                // marca error de formato
                var paramsJSON = {};
                _.extend(paramsJSON, $scope.model);

                // Convertimos los objetos Date a formato String
                paramsJSON.fecha_nacimiento = moment(paramsJSON.fecha_nacimiento, 'DD-MM-YYYY').format('YYYY-MM-DD');
                paramsJSON.vigencia_licencia_conducir = moment(paramsJSON.vigencia_licencia_conducir, 'DD-MM-YYYY').format('YYYY-MM-DD');
                
                // Transformamos el JSON a un objeto FormData
                // para poder enviarlo en el POST
                var formData = Object.toFormData(paramsJSON);
                
                // Si el formulario es correcto, procedemos a enviar los datos
                $scope.promise = true;

                // Enviamos la solicitud con un customPOST
                // para poder enviar el archivo
                Restangular.all('conductores')
                    .withHttpConfig({transformRequest: angular.identity})
                    .customPOST(formData, undefined, undefined, {'Content-Type': undefined})
                    .then(function () {
                        ToastService.success('Datos guardados exitosamente.').then(function () {
                            $state.go(resource + '_index');
                        });
                    }, function () {
                        $scope.promise = false;
                        ToastService.errorMsg();
                    });
            };

            $scope.update = function () {
                if ($scope.form.$invalid) {
                    // Establece el form como submitted para que se pueda visualizar los mensajes de error
                    $scope.form.$setSubmitted();
                    ToastService.error('Revise sus datos.');
                    return false;
                }

                // Si esta actualizando el password, lo validamos
                if ($scope.model.password != '') {
                    if ($scope.model.password != $scope.model.password_confirm) {
                        ToastService.error('Revise la contraseña, debe concidir en el campo de confirmación');
                        return false;
                    }
                }

                var paramsJSON = {};
                _.extend(paramsJSON, $scope.model);

                // Convertimos los objetos Date a formato String
                paramsJSON.fecha_nacimiento = moment(paramsJSON.fecha_nacimiento, 'DD-MM-YYYY').format('YYYY-MM-DD');
                paramsJSON.vigencia_licencia_conducir = moment(paramsJSON.vigencia_licencia_conducir, 'DD-MM-YYYY').format('YYYY-MM-DD');
                // La foto no se envia como cadena
                // sino como un objeto formData
                delete paramsJSON.foto;
                

                $scope.promise = true;
                Restangular.one('conductores', $scope.model.usuario_id)
                    .customPUT(paramsJSON).then(function(model) {
                        // Enviamos solo la foto
                        var formData = Object.toFormData({
                            foto: $scope.model.foto
                        });

                        Restangular.one('conductores', $scope.model.usuario_id)
                            .withHttpConfig({transformRequest: angular.identity})
                            .customPOST(formData, 'uploadfoto', undefined, {'Content-Type': undefined})
                            .then(function () {
                                ToastService.success('Datos guardados exitosamente.').then(function () {
                                    $state.go(resource + '_index');
                                });
                            }, function () {
                                $scope.promise = false;
                                ToastService.error('Error al subir la imagen del conductor');
                            });
                    }, function (response) {
                        $scope.promise = false;
                        ToastService.error('Error al actualizar los datos del conductor');
                    });
            };

            $scope.pageChanged = function (page, limit) {
                $scope.paginator.current_page = page;
                $scope.getList($scope.paginator.current_page);
            };

            $scope.loadTaxis = function () {
                $scope.loadingTaxis = true;
                TaxiService.getList({concesionario_id: $scope.model.taxi.concesionario_id}).then(function(taxis) {
                    $scope.taxis = taxis;
                    $scope.loadingTaxis = false;
                });
            };

            // Si la ruta que se esta ejecutando es el index
            if ($state.is(resource + '_index')) {
                // Cargamos la lista de elementos
                $scope.getList();
            }

            // Si estamos creando es necesario cargar el catálogo de municipios
            if ($state.is(resource + '_create')) {
                // Solo para el caso de registrar, es obligatorio el password
                $scope.required_password = true;

                $scope.promise = true;
                Restangular.all('municipios').getList().then(function(municipios) {
                    $scope.municipios = municipios;
                });
                Restangular.all('concesiones').getList().then(function(concesionarios) {
                    $scope.concesionarios = concesionarios;
                    $scope.promise = false;
                });
            }

            // Si estamos actualizando, es necesario obtener el elemento a editar
            if ($state.is(resource + '_edit')) {
                $scope.promise = true;
                Restangular.all('municipios').getList().then(function(municipios) {
                    $scope.municipios = municipios;
                });
                Restangular.all('concesiones').getList().then(function(concesionarios) {
                    $scope.concesionarios = concesionarios;
                });

                // El id del elemento se envía en la ruta
                ConductorService.one($state.params.id).get().then(function(model) {
                    $scope.promise = false;

                    setTimeout(function() {
                        model.numero = parseInt(model.numero);
                        model.numero_certificado_aptitud = parseInt(model.numero_certificado_aptitud);
                        // A las fechas se les suma las 6 horas de la zona horaria
                        // de lo contrario no convierte de forma correcta la fecha a string
                        //model.fecha_nacimiento = new Date(model.fecha_nacimiento + 'T06:00:00.000Z');
                        //model.vigencia_licencia_conducir = new Date(model.vigencia_licencia_conducir + 'T06:00:00.000Z');
                        model.fecha_nacimiento = moment(model.fecha_nacimiento, 'YYYY-MM-DD').format('DD-MM-YYYY');
                        model.vigencia_licencia_conducir = moment(model.vigencia_licencia_conducir, 'YYYY-MM-DD').format('DD-MM-YYYY');

                        $scope.model = model;

                        // Cargamos todos los taxis de la consecionaria para mostrarlos en el select
                        $scope.loadTaxis();
                    });
                }, function (response) {
                    ToastService.error('Error al obtener los datos, intentelo nuevamente');
                    $scope.promise = false;
                });
            }
        }
    ]);
