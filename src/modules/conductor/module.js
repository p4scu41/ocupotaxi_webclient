'use strict';
/* globals angular, Config, Helpers */

var ConductorModule = angular.module('ConductorModule', [
        'md.data.table', /* https://github.com/daniel-nagy/md-data-table */
        'ngFileUpload', /* https://github.com/danialfarid/ng-file-upload */
        'ui.mask', /* https://github.com/angular-ui/ui-mask */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', function ($stateProvider) {
        var resource = 'conductores';
        var module = 'conductor';

        $stateProvider
            .state(resource + '_index', {
                url: '/' + resource,
                templateUrl: 'src/modules/' + module + '/views/index.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_create', {
                url: '/' + resource + '/create',
                templateUrl: 'src/modules/' + module + '/views/create.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_edit', {
                url: '/' + resource + '/:id/edit',
                templateUrl: 'src/modules/' + module + '/views/edit.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];
    }]);
