'use strict'
/* globals Config, alert, _ */

var Helpers = {
    /**
     * Show a content in console when the value of Config.debug is true
     *
     * @param mixed message Content to show in console, it could be a string or object
     */
    log: function(message) {
        if (Config.debug) {
            console.log(message);
        }
    },

    // Redirecciona a la pagina de login en caso de que el usuario NO este logueado
    loginRequired: function ($q, $location, $auth) {
        var deferred = $q.defer();

        if ($auth.isAuthenticated()) {
            deferred.resolve();
        } else {
            $location.path(Config.Urls.login);
        }

        return deferred.promise;
    },

    // Detiene la ejecución del controlador en caso de que el usuario este logueado
    skipIfLoggedIn: function ($q, $auth) {
        var deferred = $q.defer();

        if ($auth.isAuthenticated()) {
            $location.path('/');
        } else {
            deferred.resolve();
        }

        return deferred.promise;
    },

    /**
     * Show information about the error on Restangular request
     *
     * @param  object response
     */
    handleErrorResponse: function (response) {
        if (response.status != -1) {
            var info = 'status: ' + response.status + '\n' +
                'statusText: ' + response.statusText + '\n' +
                'url: ' + response.config.url + '\n' +
                'method: ' + response.config.method + '\n' +
                'data: ' + JSON.stringify(response.config.data) + '\n' +
                'responseText: ';

            if (typeof response.data == 'object') {
                info += JSON.stringify(response.data);
            } else if (response.data != ''){
                info += response.data.substr(0, 1100);
            }

            console.log(info);

            alert(info);
        }
    },

    /**
     * Muestra los mensajes de error en un alert
     * @param  string|object message
     */
    showError: function (message) {
        var info = '';

        if (typeof message == 'object') {
            info += _.reduce(message, function(memo, item) {
                if (typeof item == 'object') {
                    return memo += _.reduce(item, function(acumulate, element) {
                        return acumulate + element + ' \n';
                    }, '');
                } else {
                    return memo + item + ' \n';
                }
            }, '');
        } else {
            info += message;
        }

        $.jAlert({
            'title': 'Error',
            'content': info,
            'theme': 'red',
        });
    },

    /**
     * Convierte en mayúscula la primera letra de la cadena
     *
     * @param  String string
     * @return String
     */
    capitalize: function (string) {
        return string[0].toUpperCase() + string.slice(1);
    },

    /**
     * Devuelve un JSON para mostrar como marker
     *
     * @param  double lat
     * @param  double lng
     * @param  int status
     * @param  string label
     * @param  string info
     * @return JSON Marker
     */
    markerTaxi: function(id, lat, lng, status, label, info) {
        return {
            id: id,
            idKey: id,
            icon: Config.iconsTaxi[status],
            infoWindow: info, // Contenido del infoWindow
            showWindow: false, // Bandera que determina si se muestra o no el infoWindow
            optionsWindow: {
                boxClass: 'custom-info-window'
            },
            coords: {
                latitude: lat,
                longitude: lng,
            },
            options: {
                animation: 0,
                visible: true,
                labelAnchor: '13 0',
                labelContent: label,
                labelClass: 'marker-labels'
            },
            onClick: function (marker, eventName, model) {
                model.showWindow = !model.showWindow;
            }
        };
    },

    /**
     * Devuelve un JSON para mostrar como marker
     *
     * @param  double lat
     * @param  double lng
     * @param  string label
     * @param  string info
     * @return JSON Marker
     */
    markerCliente: function(id, lat, lng, label, info) {
        return {
            id: id,
            idKey: id,
            icon: {url: 'img/PinPasajeroPanico.png'},
            infoWindow: info,
            coords: {
                latitude: lat,
                longitude: lng,
            },
            options: {
                animation: 0,
                visible: true,
                labelAnchor: '13 0',
                labelContent: label,
                labelClass: 'marker-labels'
            }
        };
    },
};