'use strict';
/* globals mainApp */

/**
 * Users Service Provider
 */
mainApp.service('ToastService', ['$mdToast', function($mdToast){
    var functions = {
        error: function (message) {
            var info = '';

            if (typeof message == 'object') {
                info += _.reduce(message, function(memo, item) { 
                    if (typeof item == 'object') {
                        return memo += _.reduce(item, function(acumulate, element) { 
                            return acumulate + element + ' \n'; 
                        }, '');
                    } else {
                        return memo + item + ' <br>';
                    }
                }, '');
            } else {
                info += message;
            }

            return $mdToast.show({
                template: '<md-toast class="md-toast error">' +
                        '<md-icon class="material-icons">highlight_off</md-icon>' +
                        'ERROR: ' + info +
                    '</md-toast>',
                hideDelay: 3000,
                position: 'top right',
                parent: angular.element('.main-container')
            });
        },
        errorMsg: function () {
            return functions.error('Error al procesar la solicitud, intentelo nuevamente');
        },
        success: function (message) {
            return $mdToast.show({
                template: '<md-toast class="md-toast success">' +
                        '<md-icon class="material-icons">done</md-icon>' +
                        message +
                    '</md-toast>',
                hideDelay: 3000,
                position: 'top right',
                parent: angular.element('.main-container')
            });
        }
    };

    return functions;
}]);