'use strict';
/* globals mainApp */

/**
 * Users Service Provider
 */
mainApp.factory('MenuService', ['$mdSidenav', '$state', 'AuthService', '$auth', '$rootScope',
    function($mdSidenav, $state, AuthService, $auth, $rootScope) {
        var itemsMenuBarNoUser = [
            {
                text: 'Iniciar Sesión',
                icon: 'input',
                state: 'login'
            }
        ];

        var itemsMenuBarUser = [
            /*{
                text: 'Perfil',
                icon: 'web',
                state: 'profile',
            },*/
            {
                text: 'Salir',
                icon: 'power_settings_new',
                state: 'logout'
            }
        ];

        var itemsSideNavUser = [
            {
                text: 'Mapa de Taxis',
                icon: 'map',
                state: 'taxis_map',
                isCurrent: function () {
                    return ($state.current.url.indexOf('taxis/map') !== -1);
                },
            },
            {
                text: 'Concesiones',
                icon: 'chrome_reader_mode',
                state: 'concesiones_index',
                isCurrent: function () {
                    return ($state.current.url.indexOf('concesiones') !== -1);
                },
            },
            {
                text: 'Taxis',
                icon: 'local_taxi',
                state: 'taxis_index',
                isCurrent: function () {
                    return ($state.current.url.indexOf('taxis') !== -1 && $state.current.url.indexOf('taxis/map') !== 1);
                },
            },
            {
                text: 'Conductores',
                icon: 'airline_seat_recline_normal',
                state: 'conductores_index',
                isCurrent: function () {
                    return ($state.current.url.indexOf('conductores') !== -1);
                },
            },
            {
                text: 'Pasajeros registrados',
                icon: 'people_outline',
                state: 'clientes_index',
                isCurrent: function () {
                    return ($state.current.url.indexOf('clientes') !== -1  && $state.current.url.indexOf('clientes/map') !== 1);
                },
            },
            {
                text: 'Pasajeros en pánico',
                icon: 'record_voice_over',
                state: 'clientes_panico',
                isCurrent: function () {
                    return ($state.current.url.indexOf('clientes/map') !== -1);
                },
            },
            {
                text: 'Usuarios del sistema',
                icon: 'supervisor_account',
                state: 'usuarios_index',
                isCurrent: function () {
                    return ($state.current.url.indexOf('usuarios') !== -1);
                },
            },
            {
                text: 'Solicitudes',
                icon: 'pan_tool',
                state: 'solicitudes_index',
                isCurrent: function () {
                    return ($state.current.url.indexOf('solicitudes') !== -1 && $state.current.url.indexOf('solicitudes/reporte') !== 1);
                },
            },
            {
                text: 'Reporte',
                icon: 'assignment',
                state: 'solicitudes_reporte',
                isCurrent: function () {
                    return ($state.current.url.indexOf('solicitudes/reporte') !== -1);
                },
            },
        ];

        var itemsSideNavNoUser = [];
        var storage = $.localStorage;

        return {
            toggle: function () {
                $mdSidenav('sidenav-left').toggle();
            },
            goto: function (state) {
                $state.go(state);
            },
            itemsMenuBar: function () {
                if ( !$auth.isAuthenticated() ) {
                    return itemsMenuBarNoUser;
                }

                return itemsMenuBarUser;
            },
            itemsSideNav: function () {
                if ( !$auth.isAuthenticated() ) {
                    return itemsSideNavNoUser;
                }

                return itemsSideNavUser;
            },
            nombreusr: function() {
                return storage.get('nombre_usr');
            },
            isOnLine: function () {
                return $rootScope.onLine;
            }
        };
    }
]);