'use strict';
/* globals angular, Config */

/**
 * @ngdoc function
 * @name mainApp.controllers:InicioController
 * @description
 * # InicioController
 * Controller of the mainApp
 */
angular.module(Config.appName)
    .controller('InicioController', ['$scope', 'MenuService', function ($scope, MenuService) {
        $scope.menu = MenuService;
    }]);
