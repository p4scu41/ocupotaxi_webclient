'use strict';
/* globals angular, Config, Helpers, window, $, _ */

/**
 * @ngdoc overview
 * @name mainApp
 * @description
 * # mainApp
 *
 * Main module of the application.
 */
var mainApp = angular.module(Config.appName, [
        'ngAnimate',
        'ngMessages',
        'ngAria',
        'ngMaterial', /* https://material.angularjs.org */
        'restangular', /* https://github.com/mgonto/restangular */
        'ui.router', /* https://github.com/angular-ui/ui-router */
        'angular-loading-bar', /* https://github.com/chieffancypants/angular-loading-bar */
        'ghiscoding.validation', /* https://github.com/ghiscoding/angular-validation */
        'pascalprecht.translate',
        'AuthModule',
        'ConcesionModule',
        'UsuarioModule',
        'TaxiModule',
        'ClienteModule',
        'SolicitudModule',
        'ConductorModule',
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', '$urlRouterProvider', 'RestangularProvider', 'cfpLoadingBarProvider', '$mdThemingProvider', '$mdIconProvider', '$translateProvider',
    function ($stateProvider, $urlRouterProvider, RestangularProvider, cfpLoadingBarProvider, $mdThemingProvider, $mdIconProvider, $translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix: 'vendor/angular-validation/locales/',
            suffix: '.json'
        });

        // define translation maps you want to use on startup
        $translateProvider.preferredLanguage('es');

        // The loading bar will only display after it has been waiting for a response for over 50ms
        cfpLoadingBarProvider.latencyThreshold = 50;
        cfpLoadingBarProvider.includeSpinner = false;

        $mdThemingProvider.theme('default')
            .primaryPalette('amber')
            .accentPalette('grey');
        //$mdThemingProvider.theme('success-toast');
        //$mdThemingProvider.theme('error-toast');

        $mdIconProvider.icon('logo', 'img/logo.svg', 128);

        // Redirect to the home state if any other states are requested
        $urlRouterProvider.otherwise(Config.Urls.login);

        $stateProvider
            .state('inicio', {
                url: '/',
                templateUrl: 'src/modules/main/views/inicio.html',
                controller: 'InicioController'
            })
            .state('users', {
                url: '/users',
                templateUrl: 'views/user/index.html',
                controller: 'UserController',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        RestangularProvider.setBaseUrl(Config.Urls.api);

        RestangularProvider.setErrorInterceptor(function(response, deferred, responseHandler) {
            // Excepciones devueltos por jwt-auth -> JSON Web Token Authentication
            switch(response.status) {
                case 400: // TokenInvalidException, InvalidClaimException
                case 401: // TokenExpiredException, TokenBlacklistedException
                //case 500: // PayloadException, JWTException
                    // Elimiminamos el token actual que es inválido
                    $.localStorage.remove($authProvider.tokenPrefix + '_' + $authProvider.tokenName);
                    Helpers.showError('ERROR: sesión expirada, inicie sesión nuevamente.');
                    window.location.reload();
                break;

                case 403: // Forbidden
                    Helpers.showError(response.data.message || 'Acceso denegado');
                break;

                case 404: // Not Found
                    Helpers.showError(response.data.message || 'Recurso no encontrado');
                break;

                case 422: // Validación de datos
                    Helpers.showError(response.data.message);
                break;
            }

            if (Config.debug) {
                Helpers.handleErrorResponse(response);
            }

            return true;  // error NO handled
        });

        RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
            var extractedData;
            /* Se recibe como respuesta este array, por lo que se tiene que
               parsear para que pueda ser procesado por Restangular
            [
                // Datos extras que se requieran
                'extra'   => null,
                // Datos solicitados
                'data'    => null,
                // Mensaje breve descriptivo de la respuesta
                'message' => null,
                // Código de la respuesta HTTP
                'status'  => null,
                // Código del error
                'code' => null
            ]*/

            if (typeof data.data != 'undefined') {
                extractedData = data.data;
                // En meta almacenaremos el resto de valores,
                // excepto data que es el contenido principal
                // revisamos que los datos sean un objeto para
                // agregar el campo meta
                if (typeof(extractedData) == 'object') {
                    extractedData.meta = _.omit(data, 'data');
                }
            } else {
                extractedData = data;
            }

            return extractedData;
        });

        // Para evitar errores al ejecutar uglify
        Helpers.skipIfLoggedIn.$inject = ['$q', '$auth'];

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];

    }]).run(['$rootScope', '$window', function($rootScope, $window) {
        $rootScope.onLine = navigator.onLine;

        $window.addEventListener('offline', function() {
            $rootScope.$apply(function() {
                $rootScope.onLine = false;
            });
        }, false);

        $window.addEventListener('online', function() {
            $rootScope.$apply(function() {
                $rootScope.onLine = true;
            });
        }, false);

        $rootScope.$on('$locationChangeStart', function(event, next, current) {
            // Cada vez que cambiamos de ruta revisamos si hay o no conexión a internet
            Offline.check();
        });
    }]);