'use strict';
/* globals angular, _, jQuery, $ */

angular.module('ClienteModule')
    .controller('ClienteController', ['$scope', '$state', 'MenuService', 'ToastService', 'ClienteService', 'uiGmapGoogleMapApi', 'Restangular', '$timeout',
        function ($scope, $state, MenuService, ToastService, ClienteService, uiGmapGoogleMapApi, Restangular, $timeout) {
            var resource = 'clientes';
            var socket = {};
            var storage = $.localStorage;
            // Bandera que nos permite saber si estabamos visualizando el mapa
            var runningMapa = $state.is(resource + '_map');

            $scope.menu = MenuService;
            $scope.model = {};
            $scope.list = [];
            $scope.filtroNombre = '';
            $scope.promise = null; // Muestra el progress-linear
            $scope.paginator = {
                'total': 0,
                'per_page': 15,
                'current_page': 1,
                'last_page': 0
            };
            $scope.map = {
                center: {  // Centramos en Tuxtla
                    latitude: 16.7516157,
                    longitude: -93.1254237
                },
                zoom: 13,
                control: {},
                markers: [],
                events: {
                    // Al hacer clic sobre el marcador
                    click: function (marker, eventName, model) {
                        // Asignamos los valores del marcador al infoWindow y lo mostramos
                        $scope.map.infoWindow.content = model.infoWindow;
                        $scope.map.infoWindow.coords = model.coords;
                        $scope.map.infoWindow.show = true;
                    }
                },
                // Instancia que guardará la información del infoWindow
                // Solo existirá uno para todos los marcadores
                // por lo que solo se podrá visualizar uno a la vez
                infoWindow: {
                    coords: {}, // Coordenadas del marcador
                    show: false, // Variabl bandera que determina si mostramos o no el inforWindow
                    content: {}, // Contenido a mostrar dentro del infoWindow
                    options: {
                        boxClass: 'custom-info-window panico',
                    },
                    close: function (event) {
                        $scope.map.infoWindow.show = false;
                    }
                }
            };

            $scope.goto = function (state, params) {
                $state.go(state, params);
            };

            $scope.getList = function(page) {
                $scope.paginator.current_page = page || 1;

                $scope.promise = ClienteService.getList({
                        page: $scope.paginator.current_page,
                        nombre: $scope.filtroNombre,
                        apellido_paterno: $scope.filtroNombre,
                        apellido_materno: $scope.filtroNombre,
                    }).then(function (list) {
                    // Copiamos los metadatos del paginador
                    $scope.paginator = _.extend($scope.paginator, _.pick(list.meta.extra, 'total', 'per_page', 'last_page'));
                    $scope.list = list;
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.toogleActivo = function (model, $event) {
                $event.stopPropagation();
                $event.preventDefault();

                model.activo = (!model.activo) ? 1 : 0;
                $scope.promise = Restangular.one('usuarios', model.id)
                    .customPUT({activo: model.activo}).then(function (model) {
                        ToastService.success('Datos actualizados exitosamente.');
                    }, function (response) {
                        ToastService.errorMsg();
                    });
            };

            $scope.pageChanged = function (page, limit) {
                $scope.paginator.current_page = page;
                $scope.getList($scope.paginator.current_page);
            };

            $scope.mapBuilder = function () {
                $scope.promise = true;

                uiGmapGoogleMapApi.then(function(maps) {
                    // Válidamos que exista el token para el servidor de tracking
                    if (storage.isEmpty('token_tracking')) {
                        ToastService.error('El token de acceso para el Servidor de Tracking no es válido, inicie sesión nuevamente.');
                        $scope.promise = false;
                        return false;
                    }

                    // Nos conectamos al servidor de tracking con el namespace admin
                    socket = io(Config.Urls.tracking + '/admin', {
                        'query': 'token=' + storage.get('token_tracking')
                    });

                    // Agregamos listener para los eventos mas comunes
                    socket.on('connect', function() {
                        console.log('socket: connect');
                    });
                    socket.on('error', function(data) {
                        ToastService.error('Error en el Servidor de Tracking.');
                        console.log('socket: error');
                        console.log(data);
                    });
                    socket.on('connect_error', function(data){
                        ToastService.error('Error en la conexión con el Servidor de Tracking.');
                        console.log('socket: connect_error');
                        console.log(data);
                    });
                    socket.on('connect_timeout', function(data){
                        ToastService.error('Error en la conexión con el Servidor de Tracking.');
                        console.log('socket: connect_timeout');
                        console.log(data);
                    });
                    socket.on('disconnect', function() {
                        console.log('socket: disconnect');
                    });

                    // Escuchamos el evento cuando un pasajero actualiza su posición
                    socket.on('posicion-pasajero-actualizada', function(data) {
                        if (typeof(data) == 'string') {
                            data = jQuery.parseJSON(data);
                        }

                        if (typeof(data.usuario_id) != 'undefined') {
                            // El wrapper timeout sirve para que se pueda actualizar la vista de
                            // los marker, de lo contrario no se vee en tiempo real los cambios
                            // de posición de cada pasajero
                            $timeout(function(){
                                // Si el pasajero ya esta en el listado
                                // actualizamos su posicion
                                var indexMarker = _.findIndex($scope.map.markers, {idKey: data.usuario_id});

                                if (indexMarker !== -1) {
                                    $scope.map.markers[indexMarker].coords.latitude = data.latitud;
                                    $scope.map.markers[indexMarker].coords.longitude = data.longitud;
                                }
                                // De lo contrario, lo insertamos al arreglo
                                else {
                                    $scope.map.markers.push(
                                        Helpers.markerCliente(
                                            data.usuario_id,
                                            data.latitud,
                                            data.longitud,
                                            data.numero_economico,
                                            data // Se envían todos los valores al infoWindow
                                        )
                                    );
                                }

                                // Si solo es un pasajero el que se esta visualizando
                                // se centra la posición del mapa en su ubicación
                                if ($scope.map.markers.length == 1) {
                                    $scope.map.center.latitude = data.latitud;
                                    $scope.map.center.longitude = data.longitud;
                                }
                            });
                        }
                    });

                    // Escuchamos el evento cuando un pasajero se desconecta
                    socket.on('pasajero-desconectado', function(data) {
                        if (typeof(data) == 'string') {
                            data = jQuery.parseJSON(data);
                        }

                        if (typeof(data.usuario_id) != 'undefined') {
                            // El wrapper timeout sirve para que se pueda actualizar la vista de
                            // los marker, de lo contrario no se vee en tiempo real los cambios
                            // de posición de cada pasajero
                            $timeout(function(){
                                var indexMarker = _.findIndex($scope.map.markers, {idKey: data.usuario_id});

                                if (indexMarker !== -1) {
                                    // Eliminamos el pasajero del array
                                    $scope.map.markers.splice(indexMarker, 1);
                                }
                            });
                        }
                    });

                    $scope.promise = false;
                });
            };

            // listener para los cambios de secciones
            $scope.$on('$locationChangeStart', function () {
                // Desconectamos el socket cuando cambiamos a una vista del mapa y
                // en caso de que este conectado
                if (runningMapa && !_.isEmpty(socket)) {
                    if (socket.connected) {
                        socket.disconnect();
                    }
                }
            });

            // Si la ruta que se esta ejecutando es el index
            if ($state.is(resource + '_index')) {
                // Cargamos la lista de elementos
                $scope.getList();
            }

            // Si estamos accediendo al mapa de pánico
            if ($state.is(resource + '_panico')) {
                $scope.promise = true;
                // Cargamos el mapa
                $scope.mapBuilder();
            }

            // Si visualizar los datos del cliente
            if ($state.is(resource + '_view')) {
                 $scope.promise = true;

                // El id del elemento se envía en la ruta
                ClienteService.one($state.params.id).get().then(function(model) {
                    $scope.promise = false;
                    $scope.model = model;
                }, function (response) {
                    ToastService.error('Error al obtener los datos, intentelo nuevamente');
                });
            }
        }
    ]);
