'use strict';
/* globals angular, Config, Helpers */

var ClienteModule = angular.module('ClienteModule', [
        'md.data.table', /* https://github.com/daniel-nagy/md-data-table */
        'uiGmapgoogle-maps', /* http://angular-ui.github.io/angular-google-maps */
        'ngFileUpload', /* https://github.com/danialfarid/ng-file-upload */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, uiGmapGoogleMapApiProvider) {
        var resource = 'clientes';
        var module = 'cliente';

        $stateProvider
            .state(resource + '_index', {
                url: '/' + resource,
                templateUrl: 'src/modules/' + module + '/views/index.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_view', {
                url: '/' + resource + '/:id/view',
                templateUrl: 'src/modules/' + module + '/views/view.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_panico', {
                url: '/' + resource + '/map',
                templateUrl: 'src/modules/' + module + '/views/mapa_panico.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];

        // Se carga la librería de google maps
        // con las opciones por default
        uiGmapGoogleMapApiProvider.configure({
            /*key: '',
            v: '3.22',
            libraries: 'weather,geometry,visualization'*/
        });
    }]);
