'use strict';

ClienteModule.service('ClienteService', ['Restangular', function(Restangular){
    return Restangular.service('pasajeros');
}]);