'use strict';
/* globals angular, _ */

angular.module('SolicitudModule')
    .controller('SolicitudController', ['$scope', '$state', 'MenuService', 'ToastService', 'SolicitudService', 'Restangular',
        function ($scope, $state, MenuService, ToastService, SolicitudService, Restangular) {
            var resource = 'solicitudes';
            $scope.menu = MenuService;
            $scope.list = [];
            $scope.filtro = {};
            $scope.filtroReporte = {};
            $scope.concesionarios = [];
            $scope.promise = null; // Muestra el progress-linear
            $scope.paginator = {
                'total': 0,
                'per_page': 15,
                'current_page': 1,
                'last_page': 0
            };
            $scope.status = new Array();
            $scope.status[0] = 'Todos';
            $scope.status[1] = 'Nuevo';
            $scope.status[2] = 'Asignado';
            $scope.status[3] = 'Abordo';
            $scope.status[4] = 'Terminado';
            $scope.status[5] = 'Cancelado';

            $scope.goto = function (state, params) {
                $state.go(state, params);
            };

            $scope.getList = function(page) {
                $scope.paginator.current_page = page || 1;
                var query = _.extend({page: $scope.paginator.current_page}, $scope.filtro);

                // Convertimos la fecha a formato MySQL
                if (typeof(query.fecha_abordaje) != 'undefined') {
                    query.fecha_abordaje = moment(query.fecha_abordaje, 'DD-MM-YYYY').format('YYYY-MM-DD');
                }

                $scope.promise = SolicitudService.getList(query).then(function (list) {
                    // Copiamos los metadatos del paginador
                    $scope.paginator = _.extend($scope.paginator, _.pick(list.meta.extra, 'total', 'per_page', 'last_page'));
                    $scope.list = list;
                }, function (response) {
                    ToastService.errorMsg();
                });
            };

            $scope.getReporte = function() {
                $scope.promise = true;
                var params = $scope.filtroReporte;

                if (typeof(params.fecha_abordaje) != 'undefined') {
                    params.fecha_abordaje = moment(params.fecha_abordaje, 'DD-MM-YYYY').format('YYYY-MM-DD');
                }

                Restangular.one('solicitudes/reporte')
                    .withHttpConfig({ responseType: 'arraybuffer' })
                    //.customGETLIST('reporte', params)
                    //.getList(params)
                    .get(params)
                    .then(function (reporte) {
                        var file = new Blob([reporte], {type: 'application/csv;charset=UTF-8;'});
                        saveAs(file, 'Reporte_'+moment().format('DD-MM-YYYY_hh-mm_a')+'.csv', false);
                        $scope.promise = false;
                    }, function (response) {
                        ToastService.errorMsg();
                        $scope.promise = false;
                    });
            };

            $scope.pageChanged = function (page, limit) {
                $scope.paginator.current_page = page;
                $scope.getList($scope.paginator.current_page);
            };

            // Si la ruta que se esta ejecutando es el index
            if ($state.is(resource + '_index')) {
                // Cargamos la lista de elementos
                $scope.getList();
            }

            if ($state.is(resource + '_reporte')) {
                // Cargamos la lista de concesionarios
                Restangular.all('concesiones').getList().then(function(concesionarios) {
                    $scope.concesionarios = concesionarios;
                });
            }
        }
    ]);
