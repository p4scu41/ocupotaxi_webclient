'use strict';

SolicitudModule.service('SolicitudService', ['Restangular', function(Restangular){
    return Restangular.service('solicitudes');
}]);