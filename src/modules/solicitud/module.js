'use strict';
/* globals angular, Config, Helpers */

var SolicitudModule = angular.module('SolicitudModule', [
        'md.data.table', /* https://github.com/daniel-nagy/md-data-table */
        'ui.mask', /* https://github.com/angular-ui/ui-mask */
    ])
    // Register the Config as a angular constant
    .constant('Config', Config)
    // Register the Helpers as a angular constant
    .constant('Helpers', Helpers)
    .config(['$stateProvider', function ($stateProvider) {
        var resource = 'solicitudes';
        var module = 'solicitud';

        $stateProvider
            .state(resource + '_index', {
                url: '/' + resource,
                templateUrl: 'src/modules/' + module + '/views/index.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            })
            .state(resource + '_reporte', {
                url: '/' + resource + '/reporte',
                templateUrl: 'src/modules/' + module + '/views/reporte.html',
                controller: Helpers.capitalize(module) + 'Controller',
                resolve: {
                    loginRequired: Helpers.loginRequired
                }
            });

        // Para evitar errores al ejecutar uglify
        Helpers.loginRequired.$inject = ['$q', '$location', '$auth'];
    }]);
